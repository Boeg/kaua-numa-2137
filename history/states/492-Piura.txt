
state={
	id=492
	name="STATE_492"
	resources={
		tungsten=2.000
	}

	history={
		owner = SIN
		buildings = {
			infrastructure = 3
			industrial_complex = 1
			air_base = 1

		}
		add_core_of = SIN
		victory_points = {
			2210 1 
		}

	}

	provinces={
		2174 2182 2210 2224 2225 5131 5166 5179 5202 5230 8157 8160 8191 8217 8225 8231 8240 10924 10938 10940 10952 10987 10996 11001 12930 12953 12973 13086 13134 13174 13175 13195 
	}
	manpower=1601278
	buildings_max_level_factor=1.000
	state_category=rural
}
