
state={
	id=368
	name="STATE_368"
	resources={
		steel=60.000
	}

	history={
		owner = TNC
		buildings = {
			infrastructure = 5
			industrial_complex = 3

		}
		add_core_of = TNC
		victory_points = {
			12501 3 
		}

	}

	provinces={
		913 968 1485 1547 1758 3994 4491 4653 5090 7012 7527 7791 8083 9949 9967 10029 10281 10657 10824 10909 12501 12670 
	}
	manpower=2616555
	buildings_max_level_factor=1.000
	state_category=city
}
