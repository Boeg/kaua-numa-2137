
state={
	id=473
	name="STATE_473"
	resources={
		oil=1.000
	}

	history={
		owner = CAS
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			dockyard = 1
			air_base = 3
			1408 = {
				naval_base = 5

			}

		}
		add_core_of = CAS
		victory_points = {
			1408 15 
		}

	}

	provinces={
		1320 1323 1347 1393 1408 1411 1690 1737 1763 1799 1832 1858 3781 4343 4385 4413 4428 4747 4778 4838 4870 4877 7414 7677 7753 7761 7820 7845 7853 9616 10155 10157 10198 10228 10258 10273 10327 10548 10602 10644 10687 10693 12124 12214 12231 12553 12571 12578 12673 12676 
	}
	manpower=638165
	buildings_max_level_factor=1.000
	state_category=town
}
