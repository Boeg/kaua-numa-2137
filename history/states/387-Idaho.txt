
state={
	id=387
	name="STATE_387"
	resources={
		steel=18.000
		chromium=1.000
		tungsten=18.000
	}

	history={
		owner = DES
		buildings = {
			infrastructure = 4

		}
		add_core_of = DES
		victory_points = {
			9616 1 
		}

	}

	provinces={
		1789 1862 7756 7827 9695 10556 10591 12535 12607 
	}
	manpower=445031
	buildings_max_level_factor=1.000
	state_category=rural
}
