
state={
	id=362
	name="STATE_362"

	history={
		owner = PRA
		buildings = {
			infrastructure = 6
			industrial_complex = 3
			dockyard = 4
			air_base = 10
			fuel_silo = 1
			788 = {
				naval_base = 10

			}

		}
		add_core_of = PRA
		victory_points = {
			788 10 
		}
		victory_points = {
			10412 15 
		}
		1939.1.1 = {
			buildings = {
				industrial_complex = 4
				dockyard = 5

			}

		}

	}

	provinces={
		788 865 873 951 1506 3823 3917 3925 3941 4627 6846 6958 6971 7466 7547 7558 7588 7646 8014 9779 9923 9931 10343 10412 10439 10441 11738 11841 11873 11888 12637 
	}
	manpower=4151054
	buildings_max_level_factor=1.000
	state_category=large_city
}
